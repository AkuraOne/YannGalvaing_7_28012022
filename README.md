# YannGalvaing_7_28012022

Création d'un réseau social d'entreprise

## Getting started

To make it easy for you to get started with GitLab, here's a list of recommended next steps.

Already a pro? Just edit this README.md and make it your own. Want to make it easy? [Use the template at the bottom](#editing-this-readme)!

## Pull and Push your files

- [ ] [Create](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#create-a-file) or [upload](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#upload-a-file) files
- [ ] [Add files using the command line](https://docs.gitlab.com/ee/gitlab-basics/add-file.html#add-a-file-using-the-command-line) or push an existing Git repository with the following command:

```
cd existing_repo
git remote add origin https://gitlab.com/AkuraOne/YannGalvaing_7_28012022.git
git branch -M main
git pull --set-upstream https://gitlab.com/AkuraOne/YannGalvaing_7_28012022 main
git push -uf origin main
```
## Before you must add MySQL configuration to your MySQL server
After MySQL installation, you must create the MySQL configuration in MySQL shell :

```
CREATE DATABASE groupomania;
USE groupomania;

CREATE USER 'groupomania'@'localhost' IDENTIFIED BY 'ez1thee3to4OoB3';
GRANT ALL PRIVILEGES ON groupomania. * TO 'groupomania'@'localhost';

SHOW GRANTS FOR groupomania@localhost;
```

## Front-end side
### Set nodeJS version of front-end side, Install NVM
First install NVM for use NodeJS multi-version with curl :
```
curl -o- https://raw.githubusercontent.com/nvm-sh/nvm/v0.39.1/install.sh | bash
```

or with wget :
```
wget -qO- https://raw.githubusercontent.com/nvm-sh/nvm/v0.39.1/install.sh | bash
```

If you want more information go here => https://github.com/nvm-sh/nvm#readme

! Under a vscode terminal place you in the root directory of the front-end ".../groupomania_front" and follow instructions. !
Don't need to indicate nodejs because nvm is Node Version Management :
```
nvm install 14.18.3
```

Don't need to indicate version of use because nvm load the .nvmrc file in front-end project :
```
nvm use
```

### Install dependencies
For install project and development dependencies :
```
npm install
```

### Launch front-end project
For launch the front-end project :
```
npm run serve
```

!!! Warning You must know the front-side default administrator account on signin view is follow :
admin@groupomania
groupomania

## Back-end side
! Under a vscode terminal place you in the root directory of the back-end ".../groupomania_back" and follow instructions. !

### Install dependencies
For install project and development dependencies :
```
npm install
```

### Configuration .env file for environment variables
- Server (NodeJS) port :
```
SERVER_PORT = 3501
```

- Server (NodeJS) address :
```
SERVER_ADDRESS = '127.0.0.1'
```

- Generated token expiration :
```
EXPIRES_IN_TOKEN = '2h'
```

- Public key of generated token :
```
TOKEN_PUBLIC_KEY = "RANDOM_ACCESS_MODE_CONTROL_SECRET"
```

- Database name :
```
DATABASE = "groupomania"
```

- Database host address :
```
DB_HOST = "127.0.0.1"
```

- Database type for Sequelize ORM :
```
DB_DIALECT = "mysql"
```

- Database user :
```
DB_USER = "groupomania"
```

- Database password created in MySQL server configuration :
```
DB_PASSWORD = "ez1thee3to4OoB3"
```

- WARNING !!! clear and initialize the Database with the datas test :
```
DB_DATAS_TESTS = false
```
Warning !!! If datas test is disabled and you had already use datas tests before, you must know the statics routes are deactivated for the datas test users avatars and it can't display itself.

- Database logs acivation : 
```
DB_DEBUG = false
```

- Logs in console activation :
```
LOG_CONSOLE = true
```

### Launch back-end project API
- For production mode :
```
npm start
```

- For development mode (using nodemon) :
```
npm run start:dev
```