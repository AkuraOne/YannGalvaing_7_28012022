
require('dotenv').config();
const express = require('express');
const app = express();
const path = require('path');
const corsNpm = require('cors');
const sequelize = require('./connection/sequelize');
const auth = require('./middleware/auth');
const initModelsAssociations = require('./models/associations');
const datasTests = require('./datasTests/datasTest');
const authRouter = require('./router/auth');
const messageRouter = require('./router/message');
const profileRouter = require("./router/profile");
const User = require('./models/User');
const bcrypt = require('bcrypt');
const {logColor,Log} = require('./controllers/console.log');
const emailer = require('./controllers/emailer');

//variables d'environnement
process.env.IMAGES_PROFILES = `http://${process.env.SERVER_ADDRESS}:${process.env.SERVER_PORT}/images/profiles/`;
const DBDebug = process.env.DB_DEBUG;
const DBDatasTests = process.env.DB_DATAS_TESTS;
const LogConsole = process.env.LOG_CONSOLE;

logColor(Log.fg.cyan,`Mode DataBase Debug : ${DBDebug}`);
logColor(Log.fg.cyan,`Mode Logs : ${LogConsole}`);
logColor(Log.fg.cyan,`Mode Datas Tests : ${DBDatasTests}`);

//Utilisation des middlewares généraux
app.use(express.json({limit:'10MB', type: 'application/json'}));
app.use(corsNpm({
    origin: '*',
    methods: ['GET','POST','PUT','PATCH','OPTIONS','DELETE','UPDATE'],
    allowedHeaders: ['Origin', 'X-Requested-With', 'Content', 'Accept', 'Content-Type', 'Authorization']
}));

const init = async ()=>{
    //Teste de la connection
    sequelize.authenticate().then(logColor(Log.fg.green,"Connection à la base de données réussie")).catch((er)=>!er ? "" : logColor(Log.fg.red, `Connection à la base de données échouée : ${er}`));

    //Synchronisation des schémas avec la base de données
    //Initialisation des relations des schémas
    await initModelsAssociations();
    await sequelize.sync({force: DBDatasTests == 'true' ? true : false});//En mode de tests force: true
    
    //Création du compte admin en premier
    const user = await User.findOne({where:{pseudo: "Admin"}});
    if(!user ){
        await User.create({pseudo:"Admin", password: await bcrypt.hash("groupomania",10), email: "admin@groupomania.fr", isAdmin: true});
    }

    //Création du jeu de données de tests
    //Initialisation du path des avatars de tests
    (DBDatasTests == 'true' ? app.use("/datasTests/images/profiles",express.static(path.join(__dirname,"/datasTests/images/profiles"))) : "");
    (DBDatasTests == 'true' ? app.use("/datasTests/images/messages",express.static(path.join(__dirname,"/datasTests/images/messages"))) : "");
    (DBDatasTests == 'true' ? datasTests() : "");
}


init();

//Création des routes de base
app.use("/images/messages",express.static(path.join(__dirname,"/images/messages")));//Pour une future amélioration
app.use("/images/profiles",express.static(path.join(__dirname,"/images/profiles")));
//app.get("/gmail",emailer);
app.use("/api/auth",authRouter);
app.use("/api/profile",auth,profileRouter);
app.use("/api/messages",auth,messageRouter);



module.exports = app;