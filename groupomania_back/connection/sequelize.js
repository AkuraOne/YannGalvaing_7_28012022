const Sequelize = require('sequelize');
const DBHost = process.env.DB_HOST;
const DBDialect = process.env.DB_DIALECT;
const DBDatabase = process.env.DATABASE;
const DBUser = process.env.DB_USER;
const DBPassword = process.env.DB_PASSWORD;
const DBDebug = process.env.DB_DEBUG;

const sequelize = new Sequelize(DBDatabase,DBUser,DBPassword,
{
    host: DBHost,
    dialect: DBDialect,
    logging: (DBDebug == 'true' ? console.log : null)
});



module.exports = sequelize;