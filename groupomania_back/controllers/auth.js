const User = require('../models/User');
const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');
const {Log,logColor} = require('../controllers/console.log');


exports.signIn = async (req,res,next)=>{
    try{
        const user = await User.findOne({where:{email: req.body.email}});

        if(!user){
            logColor(Log.fg.yellow,`Utilisateur non trouvé`);
            return res.status(404).json({message:"Utilisateur non trouvé"});
        }

        if(await bcrypt.compare(req.body.password,user.password)){
            
            const token = jwt.sign({userId:user.id},process.env.TOKEN_PUBLIC_KEY,{expiresIn:process.env.EXPIRES_IN_TOKEN});
            
            req.userId = user.id;
            logColor(Log.fg.green,`Utilisateur trouvé : ${user.pseudo}`);
            res.status(200).json({avatarURL: user.avatarURL, pseudo: user.pseudo, email: user.email, id : user.id, token: token, isAdmin: user.isAdmin});
        }
        else{
            logColor(Log.fg.yellow,`E-mail ou mot de passe incorrecte`);
            res.status(404).json({message:"E-mail ou mot de passe incorrecte"});
        }
    }
    catch(error){
        logColor(Log.fg.red,`Erreur serveur : ${error}`);
        res.status(500).json({error});
    }
}

exports.signUp = async (req,res,next)=>{
    try{
        if(await User.findOne({where:{pseudo:req.body.pseudo}})){
            logColor(Log.fg.yellow,`Pseudo déjà existant`);
            return res.status(200).json({message:"Pseudo déjà existant"});
        }

        if(await User.findOne({where:{email:req.body.email}})){
            logColor(Log.fg.yellow,`Email déjà existant`);
            return res.status(200).json({message:"Email déjà existant"});
        }

        const hashedPassword = await bcrypt.hash(req.body.password,10);

        if(req.file)
        {
            req.body.avatarURL = `${process.env.IMAGES_PROFILES}${req.file.filename}`;
        }

        const user = await User.create({avatarURL: req.body.avatarURL, pseudo:req.body.pseudo,email:req.body.email,password:hashedPassword});
        
        logColor(Log.fg.green,`Utilisateur créé`);
        res.status(201).json(user);
    }
    catch(error){
        logColor(Log.fg.red,`Erreur serveur : ${error}`);
        res.status(500).json({error});
    }
}

exports.getPseudoExist = async (req, res, next) =>{
    try{
        try{
            const user = await User.findOne({where:{pseudo:req.body.pseudo}});
            
            if(!user){
                return res.status(200).json({exist: false});
            }

            res.status(200).json({exist: true});
        }
        catch(error){
            res.status(400).json({message:`Erreur SQL : ${error}`});
        }

    }catch(error){
        res.status(500).json({message:`Erreur Serveur : ${error}`});
    }
}

exports.getEmailExist = async (req, res, next) =>{
    try{
        try{
            const user = await User.findOne({where:{email:req.body.email}});
            
            if(!user){
                return res.status(200).json({exist: false});
            }

            res.status(200).json({exist: true});
        }
        catch(error){
            res.status(400).json({message:`Erreur SQL : ${error}`});
        }

    }catch(error){
        res.status(500).json({message:`Erreur Serveur : ${error}`});
    }
}

