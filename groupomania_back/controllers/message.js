const Message = require('../models/Message');
const User = require('../models/User');
const Like = require('../models/Like');
const Dislike = require('../models/Dislike');
const { Op } = require('sequelize');
const {Log,logColor} = require('../controllers/console.log');

//Message queries
exports.getSearchAll = async (req,res,next)=>{
    try{
        var messages = [];
        
        //User comprenant un pseudo cooresspondant au texte de recherche
        const user = await User.findOne({
            where:{
                pseudo:{
                        [Op.like]: `%${req.params.searchText}%`
                }
            }
        });

        //S'il y a un user correspondant
        if(user)
        {
            messages = await Message.findAll({
                where:{ [Op.or]:[
                    {
                        body:{
                            [Op.like]: `%${req.params.searchText}%`
                        } 
                    },
                    {
                        UserId:user.id
                    }
                ]},
                order: [['date', 'DESC']],
                include: User
            });
        }
        else{//Sinon
            messages = await Message.findAll({
                where:{ 
                    body:{
                        [Op.like]: `%${req.params.searchText}%`
                    } 
                }, 
                order: [['date', 'DESC']],
                include: User
            });
        }

        //Comptage des likes et dislikes
        for(let message of messages){
            let likes = await Like.count({where:{messageId: message.id}});
            let dislikes = await Dislike.count({where:{messageId: message.id}});
            message.setDataValue('likes',likes);
            message.setDataValue('dislikes',dislikes);
        }

        if(!messages){
            logColor(Log.fg.yellow,`Aucun message`);
            return res.status(404).json({message:"Aucun message"});
        }
        else{
            return res.status(200).json(messages);
        }
    }
    catch(error){
        logColor(Log.fg.red,`Erreur serveur : ${error}`);
        res.status(500).json({message:`Erreur serveur : ${error}`});
    }
}

exports.getAll = async (req,res,next)=>{
    try{
        var messages = await Message.findAll({order: [['date', 'DESC']], include:User});

        for(let message of messages){
            let likes = await Like.count({where:{messageId: message.id}});
            let dislikes = await Dislike.count({where:{messageId: message.id}});
            message.setDataValue('likes',likes);
            message.setDataValue('dislikes',dislikes);
        }

        if(!messages){
            logColor(Log.fg.yellow,`Aucun message`);
            return res.status(404).json({message:"Aucun message"});
        }
        else{
            return res.status(200).json(messages);
        }
    }
    catch(error){
        logColor(Log.fg.red,`Erreur serveur : ${error}`);
        res.status(500).json({message:`Erreur serveur : ${error}`});
    }
}

exports.getOne= async (req,res,next)=>{
    try{
        if(!req.params.id){
            logColor(Log.fg.yellow,`Requête mal construite`);
            return res.status(400).json({message:"Requête mal construite"});
        }

        try{
            const message = await Message.findOne({where:{id:req.params.id}});
            /*let likes = await Like.count({where:{message_id: message.id}});
            let dislikes = await Dislike.count({where:{message_id: message.id}});
            message.setDataValue('likes',likes);
            message.setDataValue('dislikes',dislikes);*/

            if(!message){

                logColor(Log.fg.yellow,`Message non trouvé`);
                return res.status(404).json({message:"Message non trouvé"});
            }

            logColor(Log.fg.green,`Message trouvé`);
            res.status(200).json(message);
        }
        catch(error){
            logColor(Log.fg.red,`Erreur SQL : ${error}`);
            res.status(500).json({message:`Erreur SQL : ${error}`});
        }
    }
    catch(error){
        logColor(Log.fg.red,`Erreur serveur : ${error}`);
        res.status(500).json({message:`Erreur serveur : ${error}`});
    }
}

exports.addOne = async (req,res,next)=>{
    try{
        const user = await User.findOne({where:{id:req.auth.userId}});
        const message = await user.createMessage({body:req.body.body,date: new Date()});
        
        if(!message){
            logColor(Log.fg.yellow,`Message non crée`);
            return res.status(400).json({message:"Erreur : Message non créé"});
        }
        else{
            logColor(Log.fg.green,`Message créé`);
            res.status(201).json({message:"Message crée"});
        }
    }
    catch(error){
        logColor(Log.fg.red,`Erreur serveur : ${error}`);
        res.status(500).json({message:`Erreur serveur : ${error}`});
    }
}

exports.updateOne = async (req,res,next)=>{
    try{
        if(req.auth.userId != req.body.UserId && !req.auth.isAdmin){
            logColor(Log.fg.yellow,`Requête non authentifiée`);
            return res.status(403).json({message:"Requête non authorisée"});
        }
        
        const message = await Message.update({UserId: req.auth.userId,body:req.body.body,date: req.body.date},{where:{id:req.body.id}});
        
        if(!message){
            logColor(Log.fg.yellow,`Message non modifié`);
            return res.status(400).json({message:"Erreur : Message non modifié"});
        }
        else{
            logColor(Log.fg.green,`Message modifié`);
            res.status(201).json({message:"Message modifié"});
        }
    }
    catch(error){
        logColor(Log.fg.red,`Erreur serveur : ${error}`);
        res.status(500).json({message:`Erreur serveur : ${error}`});
    }
}

exports.deleteOne = async (req,res,next)=>{
    try{
        if(!req.params.id){
            logColor(Log.fg.red,"Requête mal construite");
            return res.status(400).json({delete: false});
        }

        const message = await Message.findOne({where:{id:req.params.id}});

        if(req.auth.userId != message.UserId && !req.auth.isAdmin){
            logColor(Log.fg.red,"Requête non autorisée. Le message n'a pas été supprimé");
            return res.status(403).json({delete: false});
        }

        try{
            await Message.destroy({where:{id:req.params.id}});
            logColor(Log.fg.green,`Message supprimé`);
            res.status(200).json({delete: true});
        }
        catch(error){
            logColor(Log.fg.red,`Erreur SQL : ${error}`);
            res.status(400).json({delete: false});
        }
    }
    catch(error){
        logColor(Log.fg.red,`Erreur Serveur : ${error}`);
        res.status(500).json({delete: false});
    }
}

//Likes queries
exports.getLike=async (req,res,next)=>{
    try{
        
        if(!req.params.userId || !req.params.messageId){
            logColor(Log.fg.red,"Requête mal construite");
            return res.status(400).json({message:"Requête mal construite"});
        }

        try{
            
            const like = await Like.findOne({where:{[Op.and]:[{userId: req.params.userId},{messageId: req.params.messageId}]}});
            
            /*if(!like){
                return res.status(404).json({like:false});
            }*/

            res.status(200).json(like);
        }
        catch(error){
            logColor(Log.fg.red,`Erreur SQL : ${error}`);
            res.status(500).json({message:`Erreur SQL : ${error}`});
        }
    }
    catch(error){
        logColor(Log.fg.red,`Erreur serveur : ${error}`);
        res.status(500).json({message:`Erreur serveur : ${error}`});
    }
}

exports.addLike = async (req,res,next) => {
    try{
        
        if(!req.body.userId || !req.body.messageId){
            logColor(Log.fg.red,"Requête mal construite");
            return res.status(400).json({message:"Requête mal construite"});
        }

        try{

            const like = await Like.create({...req.body});

            if(!like){
                logColor(Log.fg.yellow,`Like non enregistré`);
                return res.status(404).json({message:"Like non enregistré"});
            }

            logColor(Log.fg.green,`Like créé`);
            res.status(200).json(like);
        }
        catch(error){
            logColor(Log.fg.red,`Erreur SQL : ${error}`);
            res.status(500).json({message:`Erreur SQL : ${error}`});
        }
    }
    catch(error){
        logColor(Log.fg.red,`Erreur serveur : ${error}`);
        res.status(500).json({message:`Erreur serveur : ${error}`});
    }
}

exports.deleteLike=async (req,res,next)=>{
    try{
        if(!req.params.userId || !req.params.messageId){
            logColor(Log.fg.red,"Requête mal construite");
            return res.status(400).json({message:"Requête mal construite"});
        }

        try{
            await Like.destroy({where:{[Op.and]:[{userId: req.params.userId},{messageId: req.params.messageId}]}});

            logColor(Log.fg.green,`Like supprimé`);
            res.status(200).json({deleted: true});
        }
        catch(error){
            logColor(Log.fg.red,error);
            res.status(500).json({message:false});
        }
    }
    catch(error){
        logColor(Log.fg.red,`Erreur serveur : ${error}`);
        res.status(500).json({message:`Erreur serveur : ${error}`});
    }
}

//Dislikes queries
exports.getDislike=async (req,res,next)=>{
    try{
        if(!req.params.userId || !req.params.messageId){
            logColor(Log.fg.red,"Requête mal construite");
            return res.status(400).json({message:"Requête mal construite"});
        }

        try{
            const dislike = await Dislike.findOne({where:{[Op.and]:[{userId: req.params.userId},{messageId: req.params.messageId}]}});

            /*if(!likes){
                return res.status(404).json({message:"Likes non trouvé"});
            }*/

            res.status(200).json(dislike);
        }
        catch(error){
            logColor(Log.fg.red,`Erreur SQL : ${error}`);
            res.status(500).json({message:`Erreur SQL : ${error}`});
        }
    }
    catch(error){
        logColor(Log.fg.red,`Erreur serveur : ${error}`);
        res.status(500).json({message:`Erreur serveur : ${error}`});
    }
}

exports.addDislike=async (req,res,next)=>{
    try{
        if(!req.body.userId || !req.body.messageId){
            logColor(Log.fg.red,"Requête mal construite");
            return res.status(400).json({message:"Requête mal construite"});
        }

        try{
            
            const dislike = await Dislike.create({...req.body});

            if(!dislike){
                logColor(Log.fg.yellow,`Dislike non enregistré`);
                return res.status(404).json({message:"Dislike non enregistré"});
            }

            logColor(Log.fg.green,`Dislike enregistré`);
            res.status(200).json(dislike);
        }
        catch(error){
            logColor(Log.fg.red,`Erreur SQL : ${error}`);
            res.status(500).json({message:`Erreur SQL : ${error}`});
        }
    }
    catch(error){
        logColor(Log.fg.red,`Erreur serveur : ${error}`);
        res.status(500).json({message:`Erreur serveur : ${error}`});
    }
}

exports.deleteDislike=async (req,res,next)=>{
    try{
        if(!req.params.userId || !req.params.messageId){
            logColor(Log.fg.red,"Requête mal construite");
            return res.status(400).json({message:"Requête mal construite"});
        }

        try{
            await Dislike.destroy({where:{[Op.and]:[{userId: req.params.userId},{messageId: req.params.messageId}]}});
            logColor(Log.fg.green,`Dislike supprimé`);
            res.status(200).json({deleted: true});
        }
        catch(error){
            logColor(Log.fg.red,error);
            res.status(500).json({deleted: false});
        }
    }
    catch(error){
        logColor(Log.fg.red,`Erreur serveur : ${error}`);
        res.status(500).json({message:`Erreur serveur : ${error}`});
    }
}