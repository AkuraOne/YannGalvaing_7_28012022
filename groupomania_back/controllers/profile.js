const User = require("../models/User");
const bcrypt = require('bcrypt');
const { Op } = require('sequelize');
const fs = require('fs');
const Message = require("../models/Message");
const {Log,logColor} = require('../controllers/console.log');

exports.getProfile = async (req,res,next) => {
    try{
        if(!req.auth){
            logColor(Log.fg.yellow,`Requête non authentifiée`);
            return res.status(401).json({message:"Requête non authentifiée"});
        }

        if(req.params.id == req.auth.userId){
            try{
                const user = await User.findOne({where:{id:req.params.id}});
                
                if(!user){
                    logColor(Log.fg.yellow,`Utilisateur non trouvé`);
                    return res.status(404).json({message:"Utilisateur non trouvé"});
                }

                res.status(200).json(user);
            }
            catch(error){
                logColor(Log.fg.red,`Erreur SQL : ${error}`);
                res.status(400).json({message:`Erreur SQL : ${error}`});
            }
        }
        else{
            logColor(Log.fg.yellow,`Accès non autorisé`);
            res.status(403).json({message:"Accès non autorisé"});
        }
    }
    catch(error){
        logColor(Log.fg.red,`Erreur serveur : ${error}`);
        res.status(500).json({message:`Erreur Serveur : ${error}`});
    }
}

exports.deleteProfile = async (req,res,next)=>{
    try{
        
        if(!req.auth){
            logColor(Log.fg.yellow,`Requête non authentifiée`);
            return res.status(401).json({message:"Requête non authentifiée",isDeleted:false});
        }

        if(req.params.id == req.auth.userId){
            try{
                const user = await User.findOne({where:{id:req.params.id}});

                if(!user){
                    logColor(Log.fg.yellow,"Compte utilisateur inexistant");
                    return res.status(200).json({message: "Compte utilisateur inexistant",isDeleted:false});
                }

                if(user.isAdmin){
                    logColor(Log.fg.yellow,"Tentative de suppression du compte administrateur refusée");
                    return res.status(403).json({message: "Compte Administrateur",isDeleted:false});
                }

                const avatarURL = user.avatarURL;

                //suppression des messages avant celle de l'utilisateur 
                await Message.destroy({where:{UserId: user.id}});

                const resp = await User.destroy({where:{id:req.params.id}});

                if(!resp || resp == 0){
                    logColor(Log.fg.red,"Compte utilisateur non supprimé");
                    return res.status(200).json({message: "Compte utilisateur supprimé",isDeleted:false});
                }

                logColor(Log.fg.green,"Compte utilisateur supprimé");

                //Suppression de l'ancien avatar
                if(avatarURL){
                    const splitUrl = avatarURL.split('/');
                    const oldfilename = splitUrl[(splitUrl.length-1)];
                    const oldpath = `./images/profiles/${oldfilename}`;
        
                    if(fs.existsSync(oldpath)){
                        fs.rmSync(oldpath);
                        logColor(Log.fg.green,"Ancien avatar supprimé");
                    }
                }

                res.status(200).json({message: "Compte utilisateur supprimé",isDeleted:true});
            }
            catch(error){
                logColor(Log.fg.red,`Erreur SQL : ${error}`);
                res.status(400).json({message:`Erreur SQL : ${error}`,isDeleted:false});
            }
        }
        else{
            logColor(Log.fg.yellow,"Accès non autorisé");
            res.status(403).json({message:"Accès non autorisé",isDeleted:false});
        }
    }
    catch(error){
        logColor(Log.fg.red,`Erreur Serveur : ${error}`);
        res.status(500).json({message:`Erreur Serveur : ${error}`,isDeleted:false});
    }
}

exports.controlPasswordProfile = async (req,res,next)=> {
    try{
        
        if(!req.auth){
            logColor(Log.fg.yellow,`Requête non authentifiée`);
            return res.status(401).json({message:"Requête non authentifiée"});
        }

        if(req.body.id == req.auth.userId){
            const profile = await User.findOne({where:{id: req.body.id}});
            const resp = await bcrypt.compare(req.body.password, profile.password);

            res.status(200).json(resp);
        }
        else{
            logColor(Log.fg.yellow,`Accès non autorisé`);
            res.status(401).json({message:"Accès non autorisé"});
        }
    }
    catch(error){
        logColor(Log.fg.red,`Erreur Serveur : ${error}`);
        res.status(500).json({message:`Erreur Serveur : ${error}`,isDeleted:false});
    }
}

exports.updateProfile = async (req,res,next)=>{
    try{
        if(!req.auth){
            logColor(Log.fg.yellow,"Requête non authentifiée");
            return res.status(401).json({isUpdated: false});
        }

        if(req.body.id == req.auth.userId){
            try{
                const user = await User.findOne({where:{id:req.body.id}});

                if(await User.findOne({where:{[Op.and]:[{pseudo:req.body.pseudo},{id:{[Op.not]:req.body.id}}]}})){
                    logColor(Log.fg.yellow,"Compte utilisateur non mis à jour : pseudo déjà existant");
                    return res.status(304).json({message:"pseudo déjà existant"});
                }
        
                if(await User.findOne({where:{[Op.and]:[{email:req.body.email},{id:{[Op.not]:req.body.id}}]}})){
                    logColor(Log.fg.yellow,"Compte utilisateur non mis à jour : email déjà existant");
                    return res.status(304).json({message:"email déjà existant"});
                }
                
                if(req.body.password){
                    req.body.password = await bcrypt.hash(req.body.password,10);
                }

                if(req.file)
                {
                    req.body.avatarURL = `${process.env.IMAGES_PROFILES}${req.file.filename}`;
                }
                
                await user.update({...req.body});

                logColor(Log.fg.green,"Compte utilisateur mis à jour");
                res.status(200).json({isUpdated: true, filePath: req.file ? `${req.body.avatarURL}` : null});
            }
            catch(error){
                logColor(Log.fg.red,`Erreur SQL : ${error}`);
                res.status(400).json({isUpdated: false});
            }
        }
        else{
            logColor(Log.fg.yellow,"Accès non autorisé");
            res.status(401).json({isUpdated: false});
        }
    }
    catch(error){
        logColor(Log.fg.red,`Erreur Serveur : ${error}`);
        res.status(500).json({isUpdated: false});
    }
}