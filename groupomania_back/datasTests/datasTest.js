const bcrypt = require('bcrypt');
//const fs = require('fs');
const User = require('../models/User');
const Message = require('../models//Message');
const Like = require('../models/Like');
const Dislike = require('../models/Dislike');
//const fetch = require('node-fetch');

//Données de test
module.exports = async ()=>{
    

    try{
        try{
            var datas1 = "http://127.0.0.1:3501/datasTests/images/profiles/Linux.png";
            var datas2 = "http://127.0.0.1:3501/datasTests/images/profiles/neo.jpeg";
            var datas3 = "http://127.0.0.1:3501/datasTests/images/profiles/avatar.gif";

            /*let buff1 = fs.readFileSync("./images/1/Linux.png");
            var datas1 = buff1.toString("base64");
            let buff2 = fs.readFileSync("./images/2/neo.jpeg");
            var datas2 = buff2.toString("base64");
            let buff3 = fs.readFileSync("./images/3/avatar.gif");
            var datas3 = buff3.toString("base64");*/
        
            /*var datas1 = await (await fetch("http://127.0.0.1:3501/images/1/Linux.png")).blob();
            var datas2 = await (await fetch("http://127.0.0.1:3501/images/2/neo.jpeg")).blob();
            var datas3 = await (await fetch("http://127.0.0.1:3501/images/3/avatar.gif")).blob();*/
        }
        catch(error){
            console.error("Erreur serveur : ", error);
        }

        const user = await User.create({pseudo:"Akura", avatarURL: datas1, email:"webmaster@akura.fr",password:bcrypt.hashSync("azertyuiop",10)});
        const user2 = await User.create({pseudo:"Neo", avatarURL: datas2, email:"toto@akura.fr",password:bcrypt.hashSync("azertyuiop",10)});
        const user3 = await User.create({pseudo:"Titi", avatarURL: datas3, email:"titi@outlook.fr",password:bcrypt.hashSync("azertyuiop",10)});

        const msg = await user.createMessage({body: "<p>Bonjour,<br> Durant 6 mois d'apprentissage intense, de multiples heures à pratiquer, de beaucoup de plaisir, je termine bientôt cette formation de Développeur Web ! 🏆 Pendant ces 6 mois, j'ai pu découvrir le métier de développeur web et prendre connaissance des bases pour me perfectionner. Un grand merci à mon mentor, Luc Léonardi, qui m'a accompagné et précieusement conseillé durant ma formation. 🙏🏼<br> <br>"
        + "<img class='message__body__img' alt='image de Linux' src='http://localhost:3501/datasTests/images/messages/linux-retro.jpg' /></p><br><br><iframe title='Matrix : Neo contre les clones de Smith' class='message__body__video' src='https://www.youtube.com/embed/UvLQMMaVmzU'>", date: new Date()/*.toString()*/});
        const msg2 = await user2.createMessage({body: "<p>Bonjour,<br> Ceci est mon deuxième message géné auto.<br></p><img class='message__body__img' alt='image Matrix' title='Matrix GIF' src='http://localhost:3501/datasTests/images/messages/matrix.gif' />", date: new Date()/*.toString()*/});
        const msg3 = await user3.createMessage({body: "<p>Bonjour,<br> Ceci est mon premier message généré automatiquement</p><br><a href='https://www.youtube.com/watch?v=UvLQMMaVmzU'>Neo vs Smith Clones part 2</a>", date: new Date()/*.toString()*/})

        /*const like = await Like.create({userId:user2.id,messageId:msg.id});
        const like2 = await Like.create({userId:user3.id,messageId:msg.id});
        const like3 = await Like.create({userId:user.id,messageId:msg2.id});
        const dislike = await Dislike.create({userId:user3.id,messageId:msg2.id});
        const dislike2 = await Dislike.create({userId:user2.id,messageId:msg3.id});
        const dislike3 = await Dislike.create({userId:user.id,messageId:msg3.id});*/

        const like = await user2.createLike({messageId:msg.id});
        const like2 = await user3.createLike({messageId:msg.id});
        const like3 = await user.createLike({messageId:msg2.id});
        const dislike = await user3.createDislike({messageId:msg2.id});
        const dislike2 = await user2.createDislike({messageId:msg3.id});
        const dislike3 = await user.createDislike({messageId:msg3.id});
        
        /*const likes1 = await Like.count({where:{message_id:msg.id}});
        await Message.update({likes:likes1},{where:{id:msg.id}});*/

        /*const likes2 = await Like.count({where:{message_id:msg2.id}});
        await Message.update({likes:likes2},{where:{id:msg2.id}});*/

        /*const dislikes1 = await Dislike.count({where:{message_id:msg.id}});
        await Message.update({dislikes:dislikes1},{where:{id:msg.id}});*/
    }
    catch(error){
        console.error("Erreur SQL : ", error);
    }
}