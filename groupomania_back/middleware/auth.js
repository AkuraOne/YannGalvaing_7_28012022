const jwt = require('jsonwebtoken');
const {Log,logColor} = require('../controllers/console.log');

module.exports = async (req,res,next)=>{
    try{
        //Si aucune authentification possible
        if(!req.headers.authorization){
            logColor(Log.fg.yellow,`requête non autorisée`);
            return res.status(401).json({message:"requête non autorisée"});
        }

        const token = req.headers.authorization.replace("Bearer ","");

        try{
            const decodedToken = jwt.verify(token, process.env.TOKEN_PUBLIC_KEY);

            const userId = decodedToken.userId;
        
            req.auth = {userId,isAdmin: userId == 1};
            if(req.userId && req.userId != userId){
                logColor(Log.fg.yellow,`identifiant non autorisée`);
                res.status(401).json({message:"identifiant non autorisée"});
            }
            else{
                next();
            }
        }catch(error){
            logColor(Log.fg.yellow,`Erreur token expiré : ${error}`);
            return res.status(401).json({message:"Session expirée", expiredSession: true});
        }
        
    }
    catch(error){
        res.status(500).json({message:"Erreur serveur"});
    }
}