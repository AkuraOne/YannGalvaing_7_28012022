const multer = require('multer');
const fs =require('fs');
const {Log,logColor} = require('../controllers/console.log');

//définition des types mime pour extraction de l'extension du fichier
const MIME_TYPES = {
    'image/png': 'png',
    'image/jpg': 'jpg',
    'image/jpeg': 'jpg',
    'image/x-icon': 'ico',
    'image/webp': 'webp',
    'image/svg+xml': 'svg',
    'image/gif': 'gif',
    'image/bmp': 'bmp'
};


const storageAvatar = multer.diskStorage({
    destination: function (req, file, callback) {

        //Création du répertoire des images des profiles
        if(!fs.existsSync('images/profiles')){
          fs.mkdir('images/profiles',(err)=>{
                if(err){
                    logColor(Log.fg.red,err);
                }
          });
        }

        //Suppression de l'ancien avatar
        if(req.body.avatarURL){
          const splitUrl = req.body.avatarURL.split('/');

          const oldfilename = splitUrl[(splitUrl.length-1)];
          const oldpath = `images/profiles/${oldfilename}`;

          if(fs.existsSync(oldpath)){
            fs.rmSync(oldpath);
          }
        }
        
        callback(null, 'images/profiles');
    },
    filename: function (req, file, callback) {
      //Remplacement des espaces, pouvant être problématiques sur serveur, par des underscores
      const name = file.originalname.split(' ').join('_').split(`.${MIME_TYPES[file.mimetype]}`).join("") + `_${Date.now()}.${MIME_TYPES[file.mimetype]}`;
      callback(null,name);
    }
});

module.exports = multer({storage: storageAvatar}).single('avatar');