const { DataTypes } = require('sequelize');
const sequelize = require('../connection/sequelize');
const User = require('../models/User');
const Message = require('../models/Message');
const {Log,logColor} = require('../controllers/console.log');


const Like = sequelize.define('Like',{
    userId:{
        primaryKey: true,
        allowNull: false,
        type: DataTypes.INTEGER,
        references: {
          model: User, // 'Movies' would also work
          key: 'id'
        }
    },
    messageId:{
        primaryKey: true,
        allowNull: false,
        type: DataTypes.INTEGER,
        references: {
          model: Message, // 'Movies' would also work
          key: 'id'
        }
    }
},
{
    timestamps: false
});

//Le model défini doit être le model lui-même
const isOk = (Like === sequelize.models.Like);//true
(process.env.DB_DEBUG == 'true' ? logColor(isOk ? Log.fg.green : Log.fg.red,`Initialisation du model Like : ${isOk}`) : ""); 

module.exports = Like;