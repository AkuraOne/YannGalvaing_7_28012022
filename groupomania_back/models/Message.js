const { DataTypes } = require('sequelize');
const sequelize = require('../connection/sequelize');
const {logColor, Log} = require('../controllers/console.log');

const Message = sequelize.define('Message',{
    id:{
        type: DataTypes.INTEGER,
        autoIncrement: true,
        allowNull: false,
        primaryKey: true
    },
    body:{
        type: DataTypes.TEXT('long'),
        allowNull: true
    },
    date:{
        type: DataTypes.DATE,
        allowNull: false
    }
}, {
    //Options du model
});

//Le model défini doit être le model lui-même
const isOk = (Message === sequelize.models.Message);//true
(process.env.DB_DEBUG == 'true' ? logColor(isOk ? Log.fg.green : Log.fg.red,`Initialisation du model Message : ${isOk}`) : ""); 

module.exports = Message;