const {DataTypes} = require('sequelize');
const sequelize = require('../connection/sequelize');
const {logColor,Log} = require('../controllers/console.log');

const User = sequelize.define('User',{
  //Définition des attributs du model
  id:{
      type: DataTypes.INTEGER,
      autoIncrement: true,
      allowNull: false,
      primaryKey: true
  },
  avatarURL:{
    type: DataTypes.STRING,
    allowNull: true
  },
  pseudo: {
    type: DataTypes.STRING,
    allowNull: false,
    unique: true
  },
  email: {
    type: DataTypes.STRING,
    allowNull: false,
    unique: true
  },
  password:{
    type: DataTypes.STRING,
    allowNull: false
  },
  isAdmin: {
    type: DataTypes.BOOLEAN,
    defaultValue: false,
    allowNull: false
  }
}, {
  //Options du model
});

//Le model défini doit être le model lui-même
const isOk = (User === sequelize.models.User);//true
(process.env.DB_DEBUG == 'true' ? logColor(isOk ? Log.fg.green : Log.fg.red,`Initialisation du model User : ${isOk}`) : ""); 

module.exports = User;