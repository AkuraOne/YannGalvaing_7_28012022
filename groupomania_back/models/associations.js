const User = require('./User');
const Message = require('./Message');
const Like = require('./Like');
const Dislike = require('./Dislike');
//const sequelize = require('../connection/sequelize');

module.exports = async ()=>{
    
    //Relation 1 user a plusieurs Messages
    User.hasMany(Message);

    //1 message appartient à un user
    Message.belongsTo(User,{onDelete:'CASCADE',onUpdate:'CASCADE'});

    //Relation plusieurs users appartient à plusieurs messages dans la table like
    //User.belongsToMany(Message,{through:Like});
    User.hasMany(Like,{foreignKey:'userId'});
    Like.belongsTo(User,{foreignKey:'userId', as: 'user'});
    
    Message.hasMany(Like,{foreignKey:'messageId'});
    Like.belongsTo(Message,{foreignKey:'messageId', as: 'message'});

    //Relation plusieurs messages appartient à plusieurs users dans la table like
    //Message.belongsToMany(User,{through:Like});

    //Relation plusieurs users appartient à plusieurs messages dans la table dislike
    //User.belongsToMany(Message,{through:Dislike});
    User.hasMany(Dislike,{foreignKey:'userId'});
    Dislike.belongsTo(User,{foreignKey:'userId', as: 'user'});

    Message.hasMany(Dislike,{foreignKey:'messageId'});
    Dislike.belongsTo(Message,{foreignKey:'messageId', as: 'message'});
    
    //Relation plusieurs messages appartient à plusieurs users dans la table dislike
    //Message.belongsToMany(User,{through:Dislike});
    

}