const express = require('express');
const router = express.Router();
const authCtrl = require('../controllers/auth');
const uploadAvatar = require('../middleware/upAvatar');


router.post("/signin",authCtrl.signIn);
router.post("/signup",uploadAvatar,authCtrl.signUp);
router.post("/email_exist",authCtrl.getEmailExist);
router.post("/pseudo_exist",authCtrl.getPseudoExist);

module.exports = router;