const express = require('express');
const router = express.Router();
const messageCtrl = require('../controllers/message');

//Messages routes
router.get("/",messageCtrl.getAll);
router.get("/search/:searchText",messageCtrl.getSearchAll);
router.get("/:id",messageCtrl.getOne);
router.post("/",messageCtrl.addOne);
router.put("/",messageCtrl.updateOne);
router.delete("/:id",messageCtrl.deleteOne)

//Likes routes
router.get("/likes/:userId/:messageId",messageCtrl.getLike);
router.post("/likes/",messageCtrl.addLike);
router.delete("/likes/:userId/:messageId",messageCtrl.deleteLike);

//Dislikes routes
router.get("/dislikes/:userId/:messageId",messageCtrl.getDislike);
router.post("/dislikes/",messageCtrl.addDislike);
router.delete("/dislikes/:userId/:messageId",messageCtrl.deleteDislike);

module.exports = router;