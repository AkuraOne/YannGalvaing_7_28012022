const express = require('express');
const router = express.Router();
const profileCtrl = require('../controllers/profile');
const uploadAvatar = require('../middleware/upAvatar');


router.get("/:id",profileCtrl.getProfile);
router.put("/",uploadAvatar,profileCtrl.updateProfile);
router.post("/control_password",profileCtrl.controlPasswordProfile);
router.delete("/:id",profileCtrl.deleteProfile);

module.exports = router;