const app = require('./app');
const http = require('http');
const {Log,logColor} = require('./controllers/console.log');

const server = http.createServer(app);

const normalizePort = (value)=>{
    const port = parseInt(value);
    
    if(isNaN(port)){
        return value;
    }

    if(port >= 0){
        return port;
    }

    return false;
}

const port = normalizePort(process.env.SERVER_PORT);

const errorHandler = (error)=>{
    if(error.syscall !== 'listen'){
        throw error;
    }

    const address = server.address();
    const bind = typeof address === 'string' ? `http://${address}:${port}` : `http://${address.address}:${address.port}`;

    switch(error.code){
        case 'EACCESS':
            console.error("Accés non autorisé");
            process.exit(1);
            break;
        case 'EADDRINUSE':
            console.error(`${bind} déjà en cours d'utilisation`);
            process.exit(1);
            break;
        default:
            throw error;
            break;
    }
}

const listeningHandle = ()=>{
    const address = server.address();
    const bind = typeof address === 'string' ? `http://${address}:${port}` : `http://${address.address}:${address.port}`;
    
    logColor(Log.fg.cyan,`Serveur API Groupomania en ligne sur => ${bind}`);
}

server.on('error',errorHandler);
server.on('listening',listeningHandle);
server.listen(port, process.env.SERVER_ADDRESS);

