import fetch from 'node-fetch';
import store from "../store";

export default {
    async query(method,endPoint,body=null, isForm =false){
        const host = store.state.host;
        const api = store.state.api;
        const headers = store.state.headers;

        //console.log(`${method} launch to API : ${host}${api}${endPoint} headers : ${isFile ? JSON.stringify(headersFiles) : JSON.stringify(headers)} isJSON : ${isJSON} body : ${body}`);
        var resp = null;

        if(method === 'GET'){
            resp =  await fetch(`${host}${api}${endPoint}`,{
                method: method,
                mode: 'cors',
                headers: headers
            });
        }
        else{
            if(isForm){
                resp = await fetch(`${host}${api}${endPoint}`, {
                    method: method,
                    body: body ? body : "",
                    headers: {
                        'Authorization' : headers.Authorization
                    }
                });
            }
            else{
                resp =  await fetch(`${host}${api}${endPoint}`,{
                    method: method,
                    mode: 'cors',
                    body : body ? JSON.stringify(body) : "",
                    headers: headers
                });
            }
            
        }

        return resp ? resp.json() : {message: "aucune reponse"};
    },
    async get(endPoint){
        return await this.query("GET",endPoint);
    },
    async post(endPoint,body, isForm){
        return await this.query("POST",endPoint,body,isForm);
    },
    async put(endPoint, body, isForm){
        return await this.query("PUT",endPoint,body,isForm);
    },
    async delete(endPoint){
        return await this.query("DELETE",endPoint);
    }
}
