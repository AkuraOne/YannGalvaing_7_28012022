module.exports = {
	isEmail:(value)=>{
		return value && /^[a-zA-Z0-9.!#$%&’*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/.test(value);
	},
	isPseudo:(value)=>{
		return value && /^[A-Za-z0-9_-]{3,10}$/.test(value);
	}
}
