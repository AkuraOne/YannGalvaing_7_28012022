import Vue from 'vue';
import VueRouter from 'vue-router';
import Home from '../views/Home.vue';
import Signin from "../views/Signin.vue";
import Signup from "../views/Signup.vue";
import Profile from "../views/Profile.vue";
import Message from "../views/Message.vue";
import store from "../store";

Vue.use(VueRouter);

const ctrlSignin = (to,from,next)=>{
  if(store.dispatch("ctrlSignin")){
    next();
  }
  else{
    next({name:"Signin"});
  }
}

const routes = [
  {
    path:"/signin",
    name: "Signin",
    component: Signin
  },
  {
    path: '/',
    name: 'Home',
    component: Home,
    beforeEnter: ctrlSignin
  },
  {
    path:"/signup",
    name: "Signup",
    component: Signup
  },
  {
    path:"/message",
    name: "Message",
    component: Message,
    beforeEnter: ctrlSignin
  },
  {
    path:"/profile",
    name: "Profile",
    component: Profile,
    beforeEnter: ctrlSignin
  },
  {
    path: '/about',
    name: 'About',
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () => import(/* webpackChunkName: "about" */ '../views/About.vue')
  }
]

const router = new VueRouter({
  mode: 'history',
  routes
})

export default router
