import Vue from 'vue';
import Vuex from 'vuex';
import httpCtrl from "../controller/httpController";
import router from "../router";
import regexControl from "../controller/regexController";

Vue.use(Vuex); 

export default new Vuex.Store({
  state: {
    host:"http://localhost:3501/",
    api:"api/",
    user: {avatarURL: String, id: Number, pseudo: String, email: String, token: String, isAdmin: Boolean},
    headers:{
      'Content-Type': 'application/json',
      'Authorization': ``
    },
    messages:[],
    isConnected: false,
    error: "",
    typesMymes:{
      'image/png': 'png',
      'image/jpg': 'jpg',
      'image/jpeg': 'jpg',
      'image/x-icon': 'ico',
      'image/webp': 'webp',
      'image/svg+xml': 'svg',
      'image/gif': 'gif',
      'image/bmp': 'bmp'
    }
  },
  getters:{
    getMessages:(state)=>{
      return state.messages;
    },
    getUserAuth: (state)=>{
      return state.user;
    },
    getHeadersAuthorization:(state)=>{
      return state.headers.Authorization;
    },
    getIsConnected:(state)=>{
      return state.isConnected;
    },
    getError:(state)=>{
      return state.error;
    }
  },
  mutations: {
    UPDATE_MESSAGES:(state,value)=>{
      state.messages = value;
    },
    UPDATE_USER_AUTH:(state, value)=>{
      state.user = value;
    },
    UPDATE_HEADERS_AUTHORIZATION:(state,value)=>{
      state.headers.Authorization = value;
    },
    UPDATE_ISCONNECTED: (state, value)=>{
      state.isConnected = value;
    },
    UPDATE_ERROR: (state, value)=>{
      state.error = value;
    }
  },
  actions: {
    async downloadMessages(context){
      const resp = await httpCtrl.get("messages");

      await context.dispatch('controlSession',resp);
      
      context.commit("UPDATE_MESSAGES",resp);
      
    },
    async searchMessage(context,text){
     
      if(text === ""){
        await context.dispatch('downloadMessages');
        return;
      }

      const resp = await httpCtrl.get(`messages/search/${text}`);
   
      await context.dispatch('controlSession',resp);

      context.commit("UPDATE_MESSAGES",resp);
    },
    async deleteMessage(context,payload){
        let resp = await httpCtrl.delete(`messages/${payload.id}`);
        
        await context.dispatch('controlSession',resp);

        if(resp.delete){
          let messages = await context.getters.getMessages;

          for(let i = 0; i < messages.length; i++){
            if(messages[i].id == payload.id){
              messages.splice(i,1);
              break;
            }
          }

          context.commit("UPDATE_MESSAGES",messages);
        }
        
    },
    async postMessage(context,payload){
      let resp = await httpCtrl.post("messages",payload);
    
      /*if(resp.message){
        console.log(resp.message);
      }*/

      await context.dispatch('controlSession',resp);
        
      router.push("/");
    },
    async updateMessage(context,message){
      let resp = await httpCtrl.put('messages',{id:message.id,body:message.body,date: message.date, UserId:message.UserId});

      /*if(resp.message){
        console.log(resp.message);
      }*/
      
        await context.dispatch('controlSession',resp);
        
        return resp;
      
    },
    async getUserLike(context,payload){
      const resp = await httpCtrl.get(`messages/likes/${context.state.user.id}/${payload.messageId}`);

        await context.dispatch('controlSession',resp);
        
        return resp;
    },
    async getUserDislike(context,payload){
      const resp = await httpCtrl.get(`messages/dislikes/${context.state.user.id}/${payload.messageId}`);

      await context.dispatch('controlSession',resp);
      
      return resp;
    },
    async updateLike(context,payload){
      try{
        const like = await httpCtrl.get(`messages/likes/${context.state.user.id}/${payload.id}`);
        
        await context.dispatch('controlSession',like);
       
        if(like){
          let resp = await httpCtrl.delete(`messages/likes/${context.state.user.id}/${payload.id}`);
          
          if(resp.deleted){
            payload.likes -= 1;
            return false;
          }
          return true;
        }else if(!like){
          let liked = await httpCtrl.post(`messages/likes`,{userId: context.state.user.id, messageId: payload.id});

          if(liked){
            payload.likes += 1;
          }
          
          const dislike = await httpCtrl.get(`messages/dislikes/${context.state.user.id}/${payload.id}`);
          
          if(dislike){
            let resp = await httpCtrl.delete(`messages/dislikes/${context.state.user.id}/${payload.id}`);
            
            if(resp.deleted){
              payload.dislikes -= 1;
            }
          }
          return true;
        }
      }catch(error){
        console.error(`${error}`);
      }
    },
    async updateDislike(context, payload){
      try{
        const dislike = await httpCtrl.get(`messages/dislikes/${context.state.user.id}/${payload.id}`);

        await context.dispatch('controlSession',dislike);

        if(dislike){
          let resp = await httpCtrl.delete(`messages/dislikes/${context.state.user.id}/${payload.id}`);

          if(resp.deleted){
            payload.dislikes -= 1;
            return false;
          }
          return true;
        }else if(!dislike){
          let disliked = await httpCtrl.post(`messages/dislikes`,{userId: context.state.user.id, messageId: payload.id});

          if(disliked){
            payload.dislikes += 1;
          }
          
          const like = await httpCtrl.get(`messages/likes/${context.state.user.id}/${payload.id}`);

          if(like){
            let resp = await httpCtrl.delete(`messages/likes/${context.state.user.id}/${payload.id}`);
            if(resp.deleted){
              payload.likes -= 1;
            }
          }
          return true;
        }
      }
      catch(error){
        console.error(`${error}`);
      }
    },
    updateUserAuth(context, payload){
      context.commit("UPDATE_USER_AUTH", payload);
    },
    async login(context, payload){
      const obj = await httpCtrl.post("auth/signin",payload);
      
      if(obj.message){
        context.commit('UPDATE_ERROR',obj.message);
        //console.error(`error : ${obj.message}`);
      }
      else{
        context.commit("UPDATE_USER_AUTH",obj);
        context.commit('UPDATE_HEADERS_AUTHORIZATION',`Bearer ${obj.token}`);
        localStorage.setItem('user-token',JSON.stringify(context.state.user));
        context.commit("UPDATE_ISCONNECTED",true);
        context.commit('UPDATE_ERROR',"");
      
        router.push("/");
      }
    },
    logout(context){
      context.commit("UPDATE_USER_AUTH",{});
      context.commit('UPDATE_HEADERS_AUTHORIZATION',``);
      context.commit("UPDATE_ISCONNECTED",false);
      localStorage.removeItem('user-token');
      localStorage.clear();
    },
    async signup(context,payload){
      const resp = await httpCtrl.post('auth/signup', payload, true);

      return resp;
    },
    async editProfile(context, payload){
      const resp = await httpCtrl.put('profile', payload, true);

      await context.dispatch('controlSession',resp);

      if(resp.isUpdated){
        localStorage.setItem('user-token',JSON.stringify(context.state.user));
      }

    return resp;
    },
    async getPseudoExist(context,payload){
      //console.log(payload);
      const resp = await httpCtrl.post(`auth/pseudo_exist`,{pseudo:payload});
      //console.log(JSON.stringify(payload) + " : " + JSON.stringify(resp));
      return resp.exist;
    },
    async getEmailExist(context,payload){
      //console.log(payload);
      const resp = await httpCtrl.post(`auth/email_exist`,{email:payload});
      //console.log(JSON.stringify(payload) + " : " + JSON.stringify(resp));
      return resp.exist;
    },
    async getProfile(context, payload){
      const resp = await httpCtrl.get(`profile/${payload.id}`);

      await context.dispatch('controlSession',resp);
        
      return resp;
    },
    async deleteProfile(context){
      const confirmation = window.confirm("Êtes-vous certain(e)s de vouloir supprimer votre compte ?");

      if(confirmation)
      {
        const resp = await httpCtrl.delete(`profile/${context.state.user.id}`);

        await context.dispatch('controlSession',resp);

        if(resp.isDeleted){
          context.commit("UPDATE_USER_AUTH",{});
          context.commit('UPDATE_HEADERS_AUTHORIZATION',``);
          context.commit("UPDATE_ISCONNECTED",false);
          localStorage.removeItem('user-token');
          localStorage.clear();

          router.push("/signin");
        }
        else{
          context.commit('UPDATE_ERROR',resp.message);
        }
      }
      else{
        return;
      }
    },
    async controlPassword(context,payload){
      return await httpCtrl.post(`profile/control_password`,payload);
    },
    ctrlSignin(context){
      const userToken = localStorage.getItem('user-token');
      if(userToken){
        context.commit("UPDATE_USER_AUTH",JSON.parse(userToken));
        context.commit("UPDATE_HEADERS_AUTHORIZATION", "Bearer " + context.getters.getUserAuth.token);
        context.commit("UPDATE_ISCONNECTED",true);
        return true;
      }
      else{
        return false;
      }
    },
    setIsConnected(context,value){
      context.commit("UPDATE_ISCONNECTED",value);
    },
    isEmail(context,value){
      return regexControl.isEmail(value);
    },
    isPseudo(context,value){
      return regexControl.isPseudo(value);
    },
    updateError(context,payload){
      context.commit('UPDATE_ERROR',payload);
    },
    async controlSession(context,resp){
      if(!resp){ return; }
      if(resp.expiredSession){
        context.commit('UPDATE_ERROR',resp.message);
        await router.push("/signin");
        return;
      }
      else{
        return resp;
      }
    }
  },
  modules: {

  }
});
